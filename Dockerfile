FROM jookies/jasmin:latest

LABEL maintainer="nixxlab@gmail.com"

RUN apt-get update && apt-get -y upgrade && apt-get install telnet

COPY jasmin.cfg /etc/jasmin/jasmin.cfg
